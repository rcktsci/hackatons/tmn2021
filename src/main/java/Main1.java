import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        String[] tokens = str.split(",");
        List<Integer> w = Arrays.stream(tokens)
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        int result = 0;

        for (int len = 1; len <= w.size(); len += 1) {
            for (int offset = 0; offset <= w.size() - len; offset += 1) {
                int sum = w.stream()
                        .mapToInt(x -> x)
                        .skip(offset)
                        .limit(len)
                        .sum();
                if (sum == len) {
                    result += 1;
                }
            }
        }
        System.out.println(result);
    }
}

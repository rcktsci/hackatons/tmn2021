import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main9 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = "2,5,1,1,4;1,2;1,3;2,4;2,5";
        str = sc.nextLine();

        String[] tokens = str.split(";");

        List<String> typesById = Arrays.stream(tokens[0].split(","))
                .collect(Collectors.toList());

        Map<Integer, List<Integer>> paths = new HashMap<>();
        Arrays.stream(tokens)
                .skip(1)
                .map(t -> t.split(","))
                .forEach(tt -> {
                    int idSrc = Integer.parseInt(tt[0]) - 1;
                    int idDst = Integer.parseInt(tt[1]) - 1;
                    List<Integer> src = paths.computeIfAbsent(idSrc, v -> new ArrayList<>());
                    src.add(idDst);
                    List<Integer> dst = paths.computeIfAbsent(idDst, v -> new ArrayList<>());
                    dst.add(idSrc);
                });

        Set<Integer> result = new HashSet<>();

        Set<Integer> ids = IntStream.range(0, typesById.size())
                .boxed()
                .collect(Collectors.toCollection(HashSet::new));

        for (Integer idFrom : ids) {
            boolean isGoodPoint = true;
            for (Integer idTo : ids) {
                if (idFrom.equals(idTo)) {
                    continue;
                }
                // calc.
                boolean isGoodPath = isGoodPoint(idFrom, idTo, typesById, paths);
                isGoodPoint = isGoodPoint && isGoodPath;
            }
            if (isGoodPoint) {
                result.add(idFrom);
            }
        }

        System.out.println(result.size());
    }

    private static boolean isGoodPoint(
            Integer idFrom,
            Integer idTo,
            List<String> typesById,
            Map<Integer, List<Integer>> paths
    ) {
        List<String> types = new ArrayList<>();
        types.add(typesById.get(idFrom));
        types.add(typesById.get(idTo));

        List<Integer> directNeighbors = paths.getOrDefault(idFrom, Collections.emptyList());
        if (!directNeighbors.contains(idTo)) {
            // long path :(
            // maybe just 2? :)
            paths.get(idTo).stream()
                    .filter(directNeighbors::contains)
                    .map(typesById::get)
                    .forEach(types::add);
        }

        return new HashSet<>(types).size() == types.size();
    }
}

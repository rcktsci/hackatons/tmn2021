import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        int k = Integer.parseInt(str);
        List<Integer> banki = IntStream.range(1, k + 1)
                .boxed()
                .sorted(Comparator.<Integer>naturalOrder().reversed())
                .collect(Collectors.toCollection(ArrayList::new));

        int win = 0;

        for (int round = 0; round < k - 1; round += 1) {
            int b1 = banki.get(0);
            int b2 = banki.get(1);
            banki.remove(0);
            int sum = b1 + b2;
            int player = sum / 2;
            int game = sum - player;
            win += player;
            banki.set(0, game);
        }

        System.out.println(win);
    }
}

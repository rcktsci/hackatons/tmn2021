import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main7 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = "0,0,2,1,1,2";
        // str = sc.nextLine();

        List<Integer> employees = Arrays.stream(str.split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        int size = employees.size();

        int[] dir = new int[size];
        while (Arrays.stream(dir).sum() != size) {
            // where flip.
            int pos = 0;
            for (int i = 0; i < size; i++) {
                if (dir[i] == 1) {
                    pos++;
                    continue;
                }
                break;
            }
            // inc.
            dir[pos] = 1;
            for (int i = 0; i < pos; i++) { dir[i] = 0; }
            // set newbies.
            for (int i = 0; i < size; i++) { if (employees.get(i) == 0) { dir[i] = 1; } }

            // eval.
            boolean[] gotHelp = new boolean[size];
            int[] canHelp = new int[size];
            for (int i = 0; i < size; i++) { canHelp[i] = employees.get(i); }

            // help to left.
            for (int i = size - 1; i >= 0; i--) {
                if (dir[i] == 0) {
                    for (int h = i - 1; h >= 0 && canHelp[i] > 0; h--) {
                        if (!gotHelp[h]) {
                            gotHelp[h] = true;
                            canHelp[i] -= 1;
                        }
                    }
                }
            }
            // help to right.
            for (int i = 0; i < size; i++) {
                if (dir[i] == 1) {
                    for (int h = i + 1; h < size && canHelp[i] > 0; h++) {
                        if (!gotHelp[h]) {
                            gotHelp[h] = true;
                            canHelp[i] -= 1;
                        }
                    }
                }
            }

            // calc help.
            boolean decisionFound = Arrays.stream(canHelp)
                                            .allMatch(ch -> ch == 0)
                                    && IntStream.range(0, size)
                                            .mapToObj(i -> gotHelp[i])
                                            .allMatch(gh -> gh);
            if (decisionFound) {
                System.out.println(Arrays.stream(dir)
                        .mapToObj(String::valueOf)
                        .collect(Collectors.joining(",")));
                return;
            }
        }

        System.out.println("NO");
    }
}

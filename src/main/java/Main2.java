import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Main2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        String[] pairs = str.split(";");
        List<SimpleEntry<Integer, Integer>> list = Arrays.stream(pairs)
                .map(p -> p.split(","))
                .map(p -> new SimpleEntry<>(
                        Integer.parseInt(p[0]),
                        Integer.parseInt(p[1])))
                .collect(Collectors.toList());
        SimpleEntry<Integer, Integer> first = list.get(0);
        int t = first.getKey();
        int k = first.getValue();
        AtomicInteger sum = new AtomicInteger();
        AtomicInteger cnt = new AtomicInteger();
        Comparator<Map.Entry<Integer, Integer>> comparator = Map.Entry
                .<Integer, Integer>comparingByValue()
                .reversed();
        list.stream()
                .skip(1)
                .sorted(comparator)
                .forEach(e -> {
                    int buy = t - cnt.get();
                    int can = Math.min(e.getKey(), buy);
                    cnt.addAndGet(can);
                    sum.addAndGet(can * e.getValue());
                });
        System.out.println(sum.get());
    }
}
